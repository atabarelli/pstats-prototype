$(document).ready(function(){
    $("#carouselButton").click(function(){
        if ($("#carouselButton").children("span").hasClass('fa-pause')) {
            $("#mycarousel").carousel('pause');
            $("#carouselButton").children("span").removeClass('fa-pause');
            $("#carouselButton").children("span").addClass('fa-play');
        }
        else if ($("#carouselButton").children("span").hasClass('fa-play')){
            $("#mycarousel").carousel('cycle');
            $("#carouselButton").children("span").removeClass('fa-play');
            $("#carouselButton").children("span").addClass('fa-pause');                    
        }
    });

    $("#login").click(function(){
        $('#loginModal').modal('toggle');
    });

    $("#reserveTable").click(function(){
        $('#reserveTableModal').modal('toggle');
    });

    var ctx00 = document.getElementById('myChart_00').getContext('2d');
    var chart00 = new Chart(ctx00, {
        type: 'line',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'Customers per Month',
                borderColor: '#5c6bc0',
                data: [10, 5, 5, 2, 20, 45, 15]
            }]
        },

        // Configuration options go here
        options: {}
    });

    var ctx01 = document.getElementById('myChart_01').getContext('2d');
    var myRadarChart = new Chart(ctx01, {
        type: 'polarArea',
        data: {
            datasets: [{
                label: 'Types of Dishes',
                data: [10, 20, 30],
                backgroundColor: ['#5c6bc0','#7986cb','#9fa8da']
            }],
            labels: [
                'Dessert',
                'Entries',
                'Main'
            ],
        },

        options: {}
    });

    var ctx02 = document.getElementById('myChart_02').getContext('2d');
    var chart02 = new Chart(ctx02, {
        type: 'bar',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'Customers per Month',
                backgroundColor: '#7986cb',
                borderColor: '#5c6bc0',
                data: [10, 5, 5, 2, 20, 45, 15]
            }]
        },
        options: {}
    });

    var ctx10 = document.getElementById('myChart_10').getContext('2d');
    var chart10 = new Chart(ctx10, {
        type: 'pie',
        data: {
            datasets: [{
                label: 'Types of Dishes',
                data: [10, 20, 30],
                backgroundColor: ['#5c6bc0','#7986cb','#9fa8da']
            }],
            labels: [
                'Dessert',
                'Entries',
                'Main'
            ],
        },

        options: {}
    });

    var ctx11 = document.getElementById('myChart_11').getContext('2d');
    var chart11 = new Chart(ctx11, {
        type: 'radar',
        data: {
            labels: ['Morning', 'Noon', 'Afternoon', 'Night'],

            datasets: [{
                label: 'Meal Times',
                borderColor: '#5c6bc0',
                data: [12, 16, 4, 23]
            }]
        },  
        options: {}
    });

    var ctx12 = document.getElementById('myChart_12').getContext('2d');
    var chart12 = new Chart(ctx12, {
        type: 'doughnut',
        data: {
            datasets: [{
                label: 'Types of Dishes',
                data: [10, 20, 30],
                backgroundColor: ['#5c6bc0','#7986cb','#9fa8da']
            }],
            labels: [
                'Dessert',
                'Entries',
                'Main'
            ],
        },

        options: {}
    });

    //Carousel Charts
    var ctx00_car = document.getElementById('myChart_00_car').getContext('2d');
    var chart00_car = new Chart(ctx00_car, {
        type: 'line',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'Customers per Month',
                borderColor: '#5c6bc0',
                data: [10, 5, 5, 2, 20, 45, 15]
            }]
        },

        // Configuration options go here
        options: {}
    });

    var ctx01_car = document.getElementById('myChart_01_car').getContext('2d');
    var myRadarChart_car = new Chart(ctx01_car, {
        type: 'polarArea',
        data: {
            datasets: [{
                label: 'Types of Dishes',
                data: [10, 20, 30],
                backgroundColor: ['#5c6bc0','#7986cb','#9fa8da']
            }],
            labels: [
                'Dessert',
                'Entries',
                'Main'
            ],
        },

        options: {}
    });

    var ctx02_car = document.getElementById('myChart_02_car').getContext('2d');
    var chart02_car = new Chart(ctx02_car, {
        type: 'bar',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'Customers per Month',
                backgroundColor: '#7986cb',
                borderColor: '#5c6bc0',
                data: [10, 5, 5, 2, 20, 45, 15]
            }]
        },
        options: {}
    });

    var ctx10_car = document.getElementById('myChart_10_car').getContext('2d');
    var chart10_car = new Chart(ctx10_car, {
        type: 'pie',
        data: {
            datasets: [{
                label: 'Types of Dishes',
                data: [10, 20, 30],
                backgroundColor: ['#5c6bc0','#7986cb','#9fa8da']
            }],
            labels: [
                'Dessert',
                'Entries',
                'Main'
            ],
        },

        options: {}
    });

    var ctx11_car = document.getElementById('myChart_11_car').getContext('2d');
    var chart11_car = new Chart(ctx11_car, {
        type: 'radar',
        data: {
            labels: ['Morning', 'Noon', 'Afternoon', 'Night'],

            datasets: [{
                label: 'Meal Times',
                borderColor: '#5c6bc0',
                data: [12, 16, 4, 23]
            }]
        },  
        options: {}
    });

    var ctx12_car = document.getElementById('myChart_12_car').getContext('2d');
    var chart12_car = new Chart(ctx12_car, {
        type: 'doughnut',
        data: {
            datasets: [{
                label: 'Types of Dishes',
                data: [10, 20, 30],
                backgroundColor: ['#5c6bc0','#7986cb','#9fa8da']
            }],
            labels: [
                'Dessert',
                'Entries',
                'Main'
            ],
        },

        options: {}
    });
});